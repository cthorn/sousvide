#!/bin/bash
#
# RasPiBrew Setup Script
# 
# type the following commands:
# chmod +x raspibrew_setup.sh
# sudo ./raspibrew_setup.sh
# sudo reboot
#

while true; do
    read -p "Do you want to run apt-get update & apt-get upgrade? [y/n]" yn
    case $yn in
        [Yy]* ) apt-get -y update; apt-get -y upgrade; break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes [y] or no [n].";;
    esac
done

#Install pip (package installer):
apt-get -y install python-setuptools
easy_install pip

#Install PySerial
pip install pyserial

#Install Python i2c and smbus
apt-get -y install python-smbus

#Install Flask
apt-get -y install python-dev
apt-get -y install libpcre3-dev
pip install Flask

while true; do
    read -p "Do you want sousvide to run automatically on boot? [y/n]" yn
    case $yn in
        [Yy]* ) cp ./sousvide.service /etc/systemd/system;
		systemctl enable sousvide.service;
		systemctl daemon-reload;
		break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes [y] or no [n].";;
    esac
done

while true; do
    read -p "The system must be rebooted to complete the installation. Reboot now? [y/n]" yn
    case $yn in
        [Yy]* ) reboot; break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes [y] or no [n].";;
    esac
done


